﻿using JAG.DevTest2019.Model;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace JAG.DevTest2019.LeadService.Controllers
{
    public class LeadController : ApiController
    {
        private string _connectionString = @"Server=localhost\SQLEXPRESS;Database=JAG2019;Trusted_Connection=True;";

        [HttpPost]
        [ResponseType (typeof(LeadResponse))]
        [Route("api/lead/submit")]
        public HttpResponseMessage Post(Lead request)
        {
            LeadResponse response = new LeadResponse()
            {
                LeadId = 10000000 + new Random().Next(),
                IsCapped = false,
                Messages = new[] { "Thank you for submitting your details" },
                IsSuccessful = true,
                IsDuplicate = false
            };

            request.LeadId = response.LeadId;

            if (!CheckIfExists(request))
                InsertData(request);
            else
            {
                response.IsDuplicate = true;
                response.IsSuccessful = false;
                response.Messages = new[] { "duplicate on [email/ contact number]" };
            }
                

            Console.WriteLine($"Lead received {request.FirstName} {request.Surname}");

            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        private void InsertData(Lead request)
        {
            // define INSERT query with parameters
            //string query = "INSERT INTO dbo.Lead (LeadId, TrackingCode,FirstName, Lastname, ContactNumber, Email, ReceivedDateTime ) " +
            //               "VALUES (@LeadId, @TrackingCode,@FirstName, @Lastname, @ContactNumber, @Email, @ReceivedDateTime ) ";

            string insertLeadQuery = GetInsertLeadSQL();
            request.PopulateLeadRequestParmeterDictionary();

            // create connection and command
            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(insertLeadQuery, cn))
            {

                // define parameters and their values
                cmd.Parameters.Add("@FirstName", SqlDbType.VarChar, 255).Value = request.FirstName;
                cmd.Parameters.Add("@Lastname", SqlDbType.VarChar, 255).Value = request.Surname;
                cmd.Parameters.Add("@LeadId", SqlDbType.BigInt).Value = request.LeadId;
                cmd.Parameters.Add("@TrackingCode", SqlDbType.VarChar, 20).Value = "N/A";
                cmd.Parameters.Add("@Email", SqlDbType.VarChar, 150).Value = request.EmailAddress;
                cmd.Parameters.Add("@ReceivedDateTime", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@ContactNumber", SqlDbType.VarChar, 20).Value = request.ContactNumber;

                cn.Open();
                SqlTransaction sqlTran = cn.BeginTransaction();
                cmd.Transaction = sqlTran;

                int rowsAffected = 0;
                try
                {
                    rowsAffected += cmd.ExecuteNonQuery();
                    cmd.CommandText = GetInsertLeadParamSQL();
                    cmd.Parameters.Clear();

                    foreach (var item in request.LeadParameters)
                    {
                        cmd.Parameters.Add("@LeadParameterId", SqlDbType.VarChar, 150).Value = 10000000 + new Random().Next();
                        cmd.Parameters.Add("@LeadId", SqlDbType.BigInt).Value = request.LeadId;
                        cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50).Value = item.Key;
                        cmd.Parameters.Add("@Value", SqlDbType.VarChar, 200).Value = item.Value;

                        rowsAffected += cmd.ExecuteNonQuery();

                        cmd.Parameters.Clear();
                    }
                    sqlTran.Commit();
                }
                catch (Exception ex)
                {
                    sqlTran.Rollback();
                }
                finally
                {
                    cn.Close();
                }
                
            }
        }

        private bool CheckIfExists(Lead request)
        {
            // define INSERT query with parameters
            string query = "SELECT LeadId from dbo.Lead WHERE  Email = @Email OR ContactNumber = @ContactNumber";

            // create connection and command
            using (SqlConnection cn = new SqlConnection(_connectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {
                // define parameters and their values
                cmd.Parameters.Add("@Email", SqlDbType.VarChar, 255).Value = request.EmailAddress;
                cmd.Parameters.Add("@ContactNumber", SqlDbType.VarChar, 255).Value = request.ContactNumber;

                // open connection, execute INSERT, close connection
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                    return true;
                cn.Close();

                return false;
            }
        }

        private string GetInsertLeadCommand(Lead request)
        {
           return "BEGIN " +
            "declare @InitialTransCount int = @@TRANCOUNT " +
            "declare @TranName varchar(32) = OBJECT_NAME(@@PROCID) " +
            "declare @response int = 0 " +
            "begin try " +
            "if @InitialTransCount = 0 begin transaction @TranName " +
            "INSERT INTO dbo.Lead (LeadId, TrackingCode, FirstName, Lastname, ContactNumber, Email, ReceivedDateTime) " +
            "VALUES(@LeadId, @TrackingCode, @FirstName, @Lastname, @ContactNumber, @Email, @ReceivedDateTime) " +
            "INSERT INTO dbo.LeadParameter (LeadParameterId, LeadId, Name, Value) " +
            " VALUES (@LeadParameterId, @LeadId, @Name, @Value) , (@LeadParameterId, @LeadId, @Name, @Value) , (@LeadParameterId, @LeadId, @Name, @Value) " +
            //"INSERT INTO dbo.LeadParameter (LeadParameterId, LeadId, Name, Value) " +
            //" VALUES (@LeadParameterId1, @LeadId, @Name1, @Value1) " +
            //"INSERT INTO dbo.LeadParameter (LeadParameterId, LeadId, Name, Value) " +
            //" VALUES (@LeadParameterId, @LeadId, @Name2, @Value2) " +
            " if @@ERROR <> 0 begin goto errorMsg_section end " +
            "if @InitialTransCount = 0 commit transaction @TranName " +
            "set @response = 0 " +
            "errorMsg_section: " +
            "goto error_section " +
            "error_section: " +
            "if @InitialTransCount = 0 rollback transaction @TranName " +
            "set @response = 1 " +
            "end try " +
            "begin catch " +
            "if @InitialTransCount = 0 rollback transaction @TranName " +
            "set @response = 1 " +
            "end catch " +
            "SELECT  @response AS ErroCode " +
            "END";
        }

        private string GetInsertLeadSQL()
        {
            return "INSERT INTO dbo.Lead (LeadId, TrackingCode, FirstName, Lastname, ContactNumber, Email, ReceivedDateTime) " +
            "VALUES(@LeadId, @TrackingCode, @FirstName, @Lastname, @ContactNumber, @Email, @ReceivedDateTime) ";
        }

        private string GetInsertLeadParamSQL()
        {
            return "INSERT INTO dbo.LeadParameter (LeadParameterId, LeadId, Name, Value) " +
            " VALUES (@LeadParameterId, @LeadId, @Name, @Value)";
        }


    }
}
